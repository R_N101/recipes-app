import food2fork from '../data/food-2-fork';
import { FETCH_RECIPES } from './types';

// single recipe with recipe_id
// https://www.food2fork.com/api/get?key=390b67dcd15d382ebd53fd420b36c5c4&rId=35382

const loadData = food2fork[0].recipes;
// const apiKey = '390b67dcd15d382ebd53fd420b36c5c4';
// const apiUrl = `https://www.food2fork.com/api/search?key=${apiKey}`;
// const apiUrl = "https://api.edamam.com/search?q=chicken&app_id=54d791a5&app_key=874f166b7910f4a82bd4905ca3f1ac75";
const apiUrl = food2fork; //hardcode for now - api calls restricted

export const fetchRecipes = () => async dispath => {
    await fetch(apiUrl)
    // .then(res => res.json()) //removed whilst hardcoding json data
    .then(recipes => dispath({
        type: FETCH_RECIPES,
        // payload: recipes
        payload: loadData //hardcoded json object
    }))
    .catch(err => console.log("Error: ", err));  
}

// possible: use async/await - investigate