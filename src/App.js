import React, { Component } from 'react';
import './App.css';
import { Provider } from 'react-redux';
import store from './store/store';
import Search from './components/Search';
import Recipes from './components/Recipes';

import { searchRecipes } from "./actions/searchRecipes";

class App extends Component {

  searchRecipe = e => {
    e.preventDefault();
    const searchInput = e.target.elements.recipeName.value;
    store.dispatch(searchRecipes(searchInput))
  }

  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Search searchRecipe={this.searchRecipe} />
          <Recipes />
        </div>
      </Provider>
    );
  }
}

export default App;
