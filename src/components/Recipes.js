import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchRecipes } from "../actions/fetchRecipes";
// import { Row, Col, Card, CardTitle } from "react-materialize";

import { Link } from "react-router-dom";

class Recipes extends Component { 

  componentWillMount() {
    this.props.fetchRecipes();
  }

  render() {
    // food 2 fork api
    const recipeItems = this.props.recipes
    ? this.props.recipes.map((recipe, index)=> (

      <div className="col s12 m4 l3" key={index}>
        <div className="card small">
        
          <div className="card-image">
            <img src={recipe.image_url}
              alt={recipe.title} />
          </div>
          
          <div className="card-content">
            <p>
            {recipe.title.length < 20 
              ? `${recipe.title}` 
              : `${recipe.title.substring(0, 25)}...`}
            </p>
          </div>
          
          <div className="card-action">
            {/* <a href={recipe.source_url} target="_blank">Select this card</a> */}
            {/* <Link to={{ pathname: `/recipe/:${recipe.recipe_id}` }}>View Recipe</Link> */}
            <Link to={{ pathname: `/recipe/:35382` }}>View Recipe</Link>
          </div>

        </div>
      </div>

      // <Col s={12} m={4} 
      //   className='grid-example'
      //   key={index}>
      //   <Card className='small' 
      //     key={index}
      //     header={<CardTitle image={recipe.image_url}></CardTitle>}
      //     actions={[<a href={recipe.source_url}>This is a Link</a>]}>
      //     {recipe.title}
      //   </Card>
      // </Col>

    ))
    : []

    //edamam api
    // const recipeItems = this.props.recipes.hits
    // ? this.props.recipes.hits.map((hit, index) => (
    //   <Col s={12} m={4} className='grid-example'>
    //   <Card className='small'
    //     key={index}
    //     header={<CardTitle image={hit.recipe.image}></CardTitle>}
    //     actions={[<a href={hit.recipe.url}>This is a Link</a>]}>
    //     {hit.recipe.label}
    //   </Card>
    //   </Col>
    // ))
    // : []

    return (
      <div>
        <div className="row">
        </div>
        <div className="row">
          {recipeItems}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  recipes: state.data.recipes
});

// const mapDispatchToProps = (dispatch) => {
//   return {
//     fetchRecipes: (url) => dispatch(fetchRecipes(url))
//   };
// };

export default connect(mapStateToProps, {fetchRecipes})(Recipes);
