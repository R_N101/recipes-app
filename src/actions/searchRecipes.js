import food2forkSearchChicken from '../data/food-2-fork-search-chicken';
import { SEARCH_RECIPES } from './types';

const searchData = food2forkSearchChicken[0].recipes;

// const apiUrl = "https://api.edamam.com/search?q=chicken&app_id=54d791a5&app_key=874f166b7910f4a82bd4905ca3f1ac75";

//const apiKey = '390b67dcd15d382ebd53fd420b36c5c4';
//const searchUrl = `https://www.food2fork.com/api/search?key=${apiKey}&q=${keyword}&page=2 `

//hardcode for now - api calls restricted
const searchUrl = food2forkSearchChicken;

export const searchRecipes = (searchInput) => async dispath => {
    await fetch(searchUrl)
    // .then(res => res.json()) //removed whilst hardcoding json data
    .then(recipes => dispath({
        type: SEARCH_RECIPES,
        // payload: recipes
        payload: searchData //hardcoded json object
    }));  
}

// possible: use async/await - investigate