import { combineReducers } from 'redux';
import { recipesReducer } from './receipesReducer';

export default combineReducers({
    data: recipesReducer
});
