import React from 'react'
import { connect } from "react-redux";
import { searchRecipes } from "../actions/searchRecipes";

const Search = (props) => {
  return (
    <div className="row">
        <div className="col s12">
            <form onSubmit={props.searchRecipe}>
                <input type="text" 
                    name="recipeName" 
                    placeholder="Search here..." 
                    style={{"textAlign":"center"}} />
                <button>Search</button>
            </form>
        </div>
    </div>
  )
}

const mapStateToProps = state => ({
    recipes: state.data.recipes
});

export default connect(mapStateToProps, {searchRecipes})(Search);
