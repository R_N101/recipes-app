import { FETCH_RECIPES, SEARCH_RECIPES} from '../actions/types';

const initialState = {
    recipes: []
}

export function recipesReducer(state = initialState, action) {
    switch(action.type) {
        case FETCH_RECIPES:
            return {
                ...state,
                recipes: action.payload
            }
        
        case SEARCH_RECIPES:
            return {
                ...state,
                recipes: action.payload
            }
        
        default: 
        return state;
    }
}

export default recipesReducer;